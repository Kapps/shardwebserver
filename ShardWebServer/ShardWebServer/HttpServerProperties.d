﻿module ShardWebServer.HttpServerProperties;
private import core.time;
private import std.stdio;
private import std.file;
import std.exception : enforce;
import ShardTools.Logger;
import ShardTools.TimeSpan;
import std.string;

/// Determines the version of the Http protocol.
enum HttpVersion {
	/// Version 1.0
	V1,
	/// Version 1.1
	V11
}

/// Provides settings for the standalone Http server.
/// Note that this class acts as a snapshot.
/// That is, modifications after the HttpServer is created will result in undefined behaviour, depending on the propetry being changed.
class HttpServerProperties  {

public:
	/// Initializes a new instance of the HttpServerProperties object.
	this() {		
		_TimeoutDuration = dur!"seconds"(15);					
		_StaticFileExcludeExtensions = new string[2];
		_StaticFileExcludeExtensions[0] = ".d";
		_StaticFileExcludeExtensions[1] = "";		
	}

	/// Gets or sets the maximum amount of cookies a single user may have for a single request.
	/// A value that's too high may result in higher CPU usage if a user attempts to send too many, allowing denial of service attacks.
	/// The default value is 64.	
	@property size_t MaxCookiesPerRequest() const {
		return _MaxCookiesPerUser;
	}

	/// Ditto
	@property void MaxCookiesPerRequest(size_t value) {
		_MaxCookiesPerUser = value;
	}

	/// Indicates whether to support the SSL protocol.
	/// The default value is false.
	/// If true, a valid certificate and key path must be provided.	
	@property void AllowSSL(bool Value) {		
		_AllowSSL = Value;
	}

	/// Ditto
	@property bool AllowSSL() const {
		return _AllowSSL;
	}

	/// Gets or sets the path to the certificate and key files for OpenSSL.
	///	The path must exist, and is relative to the executable directory.	
	@property string SslCertificatePath() const {
		return _SslCertificatePath;
	}

	/// Ditto
	@property void SslCertificatePath(string Value) {		
		enforce(exists(Value), "The SSL certificate located at the requested path did not exist.");
		_SslCertificatePath = Value;
	}
	
	/// Ditto
	@property string SslKeyPath() const {
		return _SslKeyPath;
	}

	/// Ditto
	@property void SslKeyPath(string Value) {	
		enforce(exists(Value), "The SSL key located at the requested path did not exist.");
		_SslKeyPath = Value;
	}

	/// Indicates the amount of time to wait since the last packet before a connection is closed.	
	/// The default value is 15 seconds.
	@property void TimeoutDuration(Duration Value) {
		// TODO: Consider large files.
		this._TimeoutDuration = Value;
	}

	/// Ditto
	@property Duration TimeoutDuration() const {
		return _TimeoutDuration;
	}

	/// Gets or sets the maximum number of connections each IP can have.
	/// A value too high will enable certain denial of service attacks, such as Slowloris.
	/// The default value is 16.
	@property uint MaxConnectionsPerUser() const {
		return _MaxConnectionsPerUser;
	}

	/// Ditto
	@property void MaxConnectionsPerUser(uint Value) {
		_MaxConnectionsPerUser = Value;
	}

	/// Gets or sets the file extensions (case insensitive, dot included) that are used for dynamic pages.
	/// The default values are ".d" and "", allowing pages with no extension or a .d extension to be treated as dynamic.
	@property string[] StaticFileExcludeExtensions() {
		return _StaticFileExcludeExtensions;
	}

	/// Ditto
	@property void StaticFileExcludeExtensions(const string[] Value) {
		_StaticFileExcludeExtensions = new string[Value.length];
		for(size_t i = 0; i < Value.length; i++) {
			string Extension = toLower(Value[i]);
			enforce(Extension.length == 0 || (Extension[0] == '.' && Extension.length > 1), "Extension must either be empty or start with a dot, followed by one or more characters.");
			_StaticFileExcludeExtensions[i] = Extension;
		}
	}

	/// Gets or sets the port the server should listen on.
	/// This takes effect on next Listen.
	/// The default value is 80 (the default http port).
	@property ushort Port() const {
		return _Port;
	}

	/// Ditto
	@property void Port(ushort Value) {
		_Port = Value;
	}

	/// Gets or sets the port the server should listen on for SSL connections.
	/// This takes effect on next Listen.
	/// The default value is 443 (the default https port).
	@property ushort PortSSL() const {
		return _PortSSL;
	}

	/// Ditto
	@property void PortSSL(ushort Value) {
		_PortSSL = Value;
	}

	/// Gets or sets the amount of bytes of a file to store in memory when sending a file.
	/// The default value is one megabyte, and takes effect as of the next request.
	/// A higher value requires less reads for large files, but uses up more memory.
	@property uint FileBufferSize() {
		// TODO: Not sure if this will be used. Probably going to be handled by FileInput and SocketOutput.
		return _FileBufferSize;
	}

	/// Ditto
	@property void FileBufferSize(uint Value) {
		_FileBufferSize = Value;
	}

	/// Gets or sets the number of worker threads used to process requests and responses by the server.
	/// The default value is 0, which is the number of cores the cpu has.
	@property uint NumWorkers() const {
		return _NumWorkers;
	}

	/// Ditto
	@property void NumWorkers(uint Value) {
		_NumWorkers = Value;
	}

private:
	int _ConcurrentConnections;
	Duration _TimeoutDuration;
	string _SslCertificatePath;
	string _SslKeyPath;
	string[] _StaticFileExcludeExtensions;
	bool _AllowSSL = false;
	size_t _MaxCookiesPerUser = 64;
	ushort _Port = 80;
	ushort _PortSSL = 443;
	uint _FileBufferSize = 1024000;
	uint _NumWorkers = 0;
	uint _MaxConnectionsPerUser = 16;	
}