﻿module ShardWebServer.SslHelper;
private import ShardWebServer.Connections.HttpConnection;
private import std.socket;
private import ShardTools.ExceptionTools;
private import ShardTools.Logger;
private import core.stdc.stdio;
private import std.string;
private import std.conv;
public import OpenSSL;

mixin(MakeException("SslException", "An SSL error has occurred."));

/// Helper class to handle SSL.
static class SslHelper  {

static public:
	
	/// Reads the SSL certificate and key from the given paths, loading them into the context.
	/// Throws an exception on an error.
	/// Params:
	/// 	ctx = The ssl context.
	/// 	certPath = The path to the certificate file.
	/// 	keyPath = The path to the key file.
	bool ReadCertificateAndKey(SSL_CTX* ctx, string certPath, string keyPath) {
		void* ptr; // OpenSSL annoyance.	
		void* certificate, key;
		
		// Certificate:
		const char* certPathz = toStringz(certPath);
		version(Windows)
			ptr = cast(void*)BIO_new_file(certPathz, toStringz("r"));			
		else
			ptr = cast(void*)fopen(certPathz, toStringz("r"));
		try {			
			version(Windows)
				PEM_read_bio_X509(cast(void*)ptr, &certificate, null, null);
			else
				PEM_read_X509(cast(FILE*)ptr, &certificate, null, null);
			EnforceErrors("Unable to read certificate.", false);	
		} finally {
			version(Windows)
				BIO_free(cast(void*)ptr);				
			else
				fclose(cast(FILE*)ptr);			
		}	
			
		// Key:
		const char* keyPathz = toStringz(keyPath);
		version(Windows)
			ptr = cast(void*)BIO_new_file(keyPathz, toStringz("r"));			
		else
			ptr = cast(void*)fopen(keyPathz, toStringz("r"));
		try {
			void* PEM = null;
			version(Windows)
				PEM_read_bio_PrivateKey(cast(void*)ptr, &key, null, null);
			else
				PEM_read_PrivateKey(cast(FILE*)ptr, &key, null, null);			
			EnforceErrors("Unable to read key.", false);	
		} finally {
			version(Windows)
				BIO_free(cast(void*)ptr);				
			else
				fclose(cast(FILE*)ptr);
		}		

		if(!SSL_CTX_use_certificate(ctx, certificate) || !SSL_CTX_use_PrivateKey(ctx, key))
			EnforceErrors("Unable to apply SSL Key or Certificate.");

		if(!SSL_CTX_check_private_key(ctx))
			EnforceErrors("Private key did not match SSL certificate.");

		return true;
	}

	/// Logs all SSL errors, returning false if there was an error. Does not throw.
	/// Params:
	/// 	AdditionalInfo = Any additional info to log with the message.
	public static bool SslCheckErrors(string AdditionalInfo) {		
		size_t NextError;
		bool Any = false;
		do {
			NextError = ERR_get_error();			
			if(NextError == 0)
				return !Any;
			char* Message = ERR_error_string(NextError, null);
			string FullDetails = format("Error occurred during SSL. %s - SSL Error: %s", AdditionalInfo, Message);
			Log("errors", FullDetails);			
			Any = true;
		} while(NextError != 0);
		return !Any;
	}

	/// Helper method to call SSLCheckErrors then throw an SSLException.
	/// Params:
	/// 	AdditionalInfo = Any additional info to log with the message.
	/// 	AlwaysThrow = If true, an SSLException will be thrown even if SSLCheckErrors returns true; otherwise, no exception is thrown provided that SSLCheckErrors returns false.
	public static void EnforceErrors(string AdditionalInfo, bool AlwaysThrow = true) {
		if(!SslCheckErrors(AdditionalInfo) || AlwaysThrow)
			throw new SslException(AdditionalInfo);
	}

	/// Initializes an SSL context on a newly created socket, returning the new context or null if creation failed.
	/// Params:
	/// 	newSocket = The newly created socket to initialize SSL on.
	/// 	InitialStatus = An out parameter set to the status of the SSL handshake for the socket.
	/// 	ctx = The SSL context to use to initialize SSL on the socket.
	public static SSL* SslInitialize(SSL_CTX* ctx, Socket newSocket, out SslStatus InitialStatus ) {
		int LastError;
		SSL* ssl = SSL_new(ctx);											
		if(!ssl) {
			SslCheckErrors("Unable to create SSL details for a connection.");
			return null;			
		}
		(SSL_set_fd(ssl, newSocket.handle));
		if(!SslCheckErrors("Unable to assign socket to SSL connection."))
			return null;			
		SSL_set_accept_state(ssl);
		if((LastError = SSL_accept(ssl)) == -1) {
			int AcceptError = SSL_get_error(ssl, LastError);
			if(AcceptError == SSL_ERROR_WANT_READ || AcceptError == SSL_ERROR_WANT_WRITE)
				InitialStatus = SslStatus.WantsIO;
			else {
				SslCheckErrors("Received invalid accept code of " ~ to!string(AcceptError) ~ ".");
				return null;
			}
		} else
			InitialStatus = SslStatus.Ready;	
		return ssl;
	}

private:
}