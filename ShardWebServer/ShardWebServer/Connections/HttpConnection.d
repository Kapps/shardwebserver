﻿module ShardWebServer.Connections.HttpConnection;
private import core.sync.mutex;
private import ShardTools.LinkedList;
private import ShardWeb.HttpRequest;
private import ShardNet.NetConnection;
import OpenSSL;
import std.datetime;
import std.socket;
import std.conv : to;
import std.stdio : writefln;
static import core.time;

enum SslStatus {
	Unknown = 0,
	NotSSL = 1,
	WantsIO = 2,
	Ready = 4
}

/// Represents a single connection to the server.
final class HttpConnection : NetConnection {

public:
	/// Initializes a new instance of the HttpConnection object.
	/// Params:	
	/// 	ssl = Provides information about the SSL protocol for this connection, or null if this connection is not using SSL.
	this(AsyncSocket Socket) { //, SSL* ssl) {		
		super(Socket);		
		Requests = new typeof(Requests)();
		/+this._ssl = ssl;
		if(ssl)
			_Status = SslStatus.Unknown;
		else
			_Status = SslStatus.NotSSL;+/
	}

	/+ /// Gets a value indicating whether this connection uses SSL.
	@property bool IsSSL() const {
		return _ssl !is null;
	}

	/// Provides SSL properties for this connection, or null if this connection does not use SSL.
	@property SSL* SSLInfo() {
		return _ssl;
	}

	/// Gets the status of the SSL handshake or protocol for this connection.
	@property SslStatus SSLStatus() const {
		return _Status;
	}

	/// Ditto
	@property void SSLStatus(SslStatus Value) {
		_Status = Value;
	}+/

protected:

	override void OnDisconnect(void* State, string Reason, int PlatformCode) {
		synchronized(this) {
			super.OnDisconnect(State, Reason, PlatformCode);
			foreach(HttpRequest Request; Requests) {
				Request.Terminate();
			}
			Requests = new typeof(Requests)();
		}
	}

private:		
	/+package SSL* _ssl;
	SslStatus _Status;	+/
	LinkedList!HttpRequest Requests; // To efficiently terminate their requests on disconnect.
} 