﻿module ShardWebServer.HttpServer;
private import std.stdio;
private import core.atomic;
private import std.variant;
private import std.typecons;
private import ShardNet.NetServer;
private import core.sync.mutex;
private import ShardTools.BufferPool;
private import ShardTools.Buffer;
private import ShardTools.StringTools;
private import std.parallelism;
private import ShardWeb.DefaultResponseCodeHandler;
private import ShardNet.BackendConnection;
private import std.string;
private import ShardTools.ExceptionTools;
import std.string : toStringz;
import ShardTools.Compressor;
import ShardTools.Timer;
import ShardWeb.HttpUtils;
import ShardTools.DateParse;
import ShardWeb.ErrorHandler;
import ShardWeb.SiteManager;
import ShardWeb.Site;
import ShardTools.PathTools;
import std.file : timeLastModified, read, exists;
import ShardWeb.HttpResponse;
import std.array;
import std.datetime;
import ShardWebServer.Connections.HttpConnection;
import ShardWebServer.HttpServerProperties;
public import ShardNet.IRequestHandler;
import std.socket;
import std.string : strip, indexOf, icmp;
import std.stdio : writefln;
import std.conv;
import ShardTools.Logger;
import std.uri;
import std.path;
import std.zlib;
import std.exception;
import ShardWeb.StaticFileCache;
import ShardTools.ArrayOps : Count, IndexOf, Contains, Any;
import core.stdc.stdio;
import OpenSSL;
import core.stdc.stdlib;
import std.datetime;
import core.time;
import ShardWebServer.SslHelper;
import ShardTools.Untyped;

alias std.string.split split;

mixin(MakeException("InsecureCookieException", "Attempted to set a SecureOnly cookie from an insecure connection."));

/// Event args for when an HttpRequest is received.
class HttpRequestReceivedEventArgs {

	/// The request being received.
	const HttpRequest Request;

	/// Whether the request was canceled.
	@property bool Canceled() {
		return _CancelReason !is null;
	}

	/// The reason the request was canceled.
	@property string CancelReason() {
		return _CancelReason;
	}

	/// Cancels this request for the given reason.
	/// Params:
	/// 	Reason = The reason to cancel this request.
	void CancelRequest(string Reason) {
		enforce(!Canceled, "Unable to cancel an already canceled request.");
		this._CancelReason = Reason;
	}

	/// Initializes a new EventArgs for an HttpRequest receive.
	/// Params:
	/// 	Request = The request being received.
	this(HttpRequest Request) {
		this.Request = Request;		
		this._CancelReason = null;
	}
	
	string _CancelReason;
}

/// Represents a stand-alone Http server.
class HttpServer  {

	// TODO: Support SSL in the IO library and in NetServer.
	// TODO: Refactor
	// TODO: Clean up the imports, damn.

	alias Event!(void, HttpRequestReceivedEventArgs) RequestReceivedEvent;
	alias Event!(void, HttpResponse) ResponseEvent;	
	alias Event!(void, HttpRequest) RequestEvent;

public:
	/// Initializes a new instance of the HttpServer object.
	this(HttpServerProperties Properties) {		
		this.Properties = Properties;				
		this._RequestReceived = new RequestReceivedEvent();
		this._ResponseSent = new ResponseEvent();
		this._RequestCancelled = new RequestEvent();
		foreach(Site site; SiteManager.AllSites)
			OnSiteAdded(site);
		SiteManager.SiteAdded ~= &OnSiteAdded;		
		this.RequestToInfoLock = new Mutex();
		// TODO: Add IPv6 support in address to connection count.
		this._Server = new NetServer!(HttpConnection)(NetProtocol.TCP, IPVersion.Both);
		_Server.MaxConnectionsPerIP = Properties.MaxConnectionsPerUser;
	}

	/// An event used for when a request is received.
	@property RequestReceivedEvent RequestReceived() {
		return _RequestReceived;
	}

	/// An event called when a response is sent.
	@property ResponseEvent ResponseSent() {
		return _ResponseSent;
	}

	/// An event called when a response was cancelled before being sent.
	@property RequestEvent RequestCancelled() {
		return _RequestCancelled;
	}

	/// Gets the properties used to determine how the server handles connections and requests.
	/// Setting these properties after the server has begun listening will have undefined results.
	@property HttpServerProperties ServerProperties() {
		return Properties;
	}		
	
	/// Begins listening for connections.
	void Listen() {
		synchronized {			
			enforce(!IsListening, "HttpServer already listening.");
			IsListening = true;
			// TODO: Check if IPv6 works after getting firmware that supports it.
			this._Server = new NetServer!HttpConnection(NetProtocol.TCP, IPVersion.Both); 
			//this._Server = new NetServer!HttpConnection(NetProtocol.TCP, IPVersion.IPv4);			
			_Server.DataReceived.Add(&OnDataReceived);
			_Server.Listen(Properties.Port);
			if(Properties.AllowSSL) {
				writefln("SSL temporarily not supported.");
				version(None) {
					VerifySSL();
					// TODO: Create a memory BIO and use that for SSL. Build this support into NetServer / NetClient. Or just NetConnection. Or derive an SslConnection.
					_Server.Listen(Properties.PortSSL);
				}
			}			
			writefln("Started listening. Non-SSL Port: %d", Properties.Port);
		}
	}	

	private void VerifySSL() {		
		enforce(Properties.SslCertificatePath, "Certificate path not set. Can not enable SSL until set.");
		enforce(Properties.SslKeyPath, "Key path not set. Can not enable SSL until set.");		
	}

	/// Stops listening for connections, closing all existing ones.
	void Close() {
		synchronized {			
			enforce(IsListening, "Can not close while not listening.");
			_Server.Disconnect();			
			IsListening = false;
			writefln("Stopped listening.");
		}
	}

protected:

	// -- CONNECTION HANDLING -- \\	

	protected void OnDataReceived(HttpConnection Connection, ubyte[] Data) {						
		char[] Message = cast(char[])Data;								
		//Connection.Socket.Send(cast(ubyte[])"HTTP/1.1 200 OK\r\nContent-Length: 11\r\nConnection: Keep-Alive\r\n\r\nHello World", null, null);
		//_Pool.put(task(&HandleReceive, Connection, Message));		
		//taskPool.put(task(&HandleReceive, Connection, Message));
		HandleReceive(Connection, Message);
	}	
	
	protected void OnResponseReceived(RequestInfo* Info) {
		IResponse Response = Info.Response;
		HttpResponse http = cast(HttpResponse)Response;		
		enforce(http !is null, "Response was not an HttpResponse.");		
		// TODO: Consider using the DefaultResponseCodeHandler global instance to handle the response. But at that point, it becomes the server doing work.
		// Because the server is a master server, and backends are all limited by the speed of the master server, we want to avoid that.		
		// For now, we'll do it anyways because it should be fairly cheap. But, perhaps change this later.
		if(!HttpUtils.IsSuccessfulStatusCode(Response.StatusCode)) { // Note: Assumes that default handler is site independent. Which it should be, as of current docs.
			Variant[string] AdditionalInfo; // Can't use inline AA variants?
			AdditionalInfo["StatusCode"] = http.StatusCode;
			AdditionalInfo["URI"] = http.Request.UriRequested;
			AdditionalInfo["Site"] = Info.SiteContaining;
			Response = DefaultResponseCodeHandler.Global.HandleUnsuccessfulResponse(http.Request, http, AdditionalInfo);
		}
		SendResponse(Info.Connection, cast(HttpResponse)Info.Response);
	}

	protected void HandleReceive(HttpConnection Connection, char[] Received) {			
		Address RemoteAddr = Connection.Socket.RemoteAddress;
		StringMap ProtocolParams, QueryParams;
		Cookie[] Cookies;
		string FailReason;
		if(!ParseInput(Connection, Received, ProtocolParams, QueryParams, Cookies, FailReason)) {					
			SendStatusCode(Connection,  new HttpRequest(null, new StringMap(), new StringMap(), RemoteAddr, null, RequestType.Dynamic), 400, "Unable to parse input parameters.\r\nDetails: " ~ FailReason);
			return;
		}
		string RequestURI = ProtocolParams.Get!string("uri");
		if(!RequestURI) {
			SendStatusCode(Connection, new HttpRequest(null, new StringMap(), new StringMap(), RemoteAddr, null, RequestType.Dynamic), 400, "Either your browser does not support the Host header, or the URL you are trying is malformed.");
			return;
		}
		string UriExtension = PathTools.GetExtension(RequestURI);
		bool IsStaticFile = !Contains(Properties.StaticFileExcludeExtensions, UriExtension);		
		HttpRequest Request = new HttpRequest(RequestURI, QueryParams, ProtocolParams, RemoteAddr, Cookies, IsStaticFile ? RequestType.Static : RequestType.Dynamic);		
		if(Request.Method != HttpMethod.Get && Request.Method != HttpMethod.Post) {		
			SendStatusCode(Connection, Request, 405, "Only Get and Post are supported.");
			return;
		}
		if(!HttpUtils.ValidateURI(RequestURI)) {
			SendStatusCode(Connection, Request, 400, "Invalid URI.");
			return;
		}
		// TODO: Provide support for looking up host name, for things like http://localhost or http://70.76.47.214
		string RequestedHost = Request.Headers.Get!string("host");
		if(!RequestedHost) {
			SendStatusCode(Connection, Request, 400, "Either no host header was found, or the header was malformed.");
			return;
		}

		Site CurrentSite = SiteManager.GetSite(RequestedHost);
		if(CurrentSite is null) {
			SendStatusCode(Connection, Request, 404, "Did not find a site for the requested host name.");
			Log("warnings", "User attempted to connect to " ~ RequestedHost ~ ", which was not found.");
			return;
		}				
		HttpRequestReceivedEventArgs EventArgs = new HttpRequestReceivedEventArgs(Request);
		this.RequestReceived.Execute(EventArgs);		
		RequestInfo* req = new RequestInfo(); //cast(RequestInfo*)malloc(RequestInfo.sizeof);
		req.Request = Request;
		req.Connection = Connection;
		req.SiteContaining = CurrentSite;
		req.Created = req.LastUpdate = Clock.currTime();
		synchronized(RequestToInfoLock) {
			RequestToInfo[Request] = req;
		}
		HandleRequest(req);				
	}

	// --- SITES AND BACKENDS --- \\

	protected void OnSiteRemoved(Site site) {
		site.Backends.Added.Remove(&OnBackendAdded);
		site.Backends.Removed.Remove(&OnBackendRemoved);
		foreach(ref BackendConnection Backend; site.Backends)
			OnBackendRemoved(site.Backends, Backend);
	}

	protected void OnSiteAdded(Site site) {
		site.Backends.Added ~= &OnBackendAdded;
		site.Backends.Removed ~= &OnBackendRemoved;
		foreach(ref BackendConnection Backend; site.Backends)
			OnBackendAdded(site.Backends, Backend);
	}

	protected void OnBackendRemoved(BackendCollection backends, BackendConnection backend) {
		enforce(backend.ResponseReceived.Remove(&OnResponseReceivedInitial));
	}

	protected void OnBackendAdded(BackendCollection backends, BackendConnection backend) {
		backend.ResponseReceived.Add(&OnResponseReceivedInitial);
	}
	 
private: 			
	size_t RequestCount;
	bool IsListening;
	NetServer!HttpConnection _Server;	
	HttpServerProperties Properties;	

	RequestReceivedEvent _RequestReceived;
	ResponseEvent _ResponseSent;
	RequestEvent _RequestCancelled;		

	RequestInfo*[IRequest] RequestToInfo;
	Mutex RequestToInfoLock;

	struct RequestInfo {
		IRequest Request;
		IResponse Response;
		HttpConnection Connection;		
		BackendConnection Backend;
		Site SiteContaining;
		SysTime Created;
		SysTime LastUpdate;
	}			

	private void OnResponseReceivedInitial(IResponse Response) {		
		//_Pool.put(task(&OnResponseReceivedWrapper, Response));		
		OnResponseReceivedWrapper(Response);
	}

	private void OnResponseReceivedWrapper(IResponse Response) {
		try {			
			RequestInfo* Info;
			synchronized(RequestToInfoLock) {
				Info = RequestToInfo.get(Response.Request, null);
				enforce(Info !is null, "Internal error, no request info for a request.");
			}
			Info.Response = Response;
			OnResponseReceived(Info);
		} catch (Throwable t) {
			Log("errors", "Error when handling response:\r\n" ~ to!string(t));
		}
	}

	private void EnsureSSLInitialized() {		
		static __gshared bool SSLInit = false;
		if(SSLInit)
			return;
		SSLInit = true;
		version(Windows) {
			// Apparently if we just use BIO instead of FILE* we can avoid the errors this causes.
			//CRYPTO_set_mem_functions(&malloc, &realloc, &free);
			//void** Ptr = OPENSSL_AppLink();		
		}
		SSL_library_init();				
		SSL_load_error_strings();
		writefln("Initialized OpenSSL library and context.");
	}

	private SSL_CTX* CreateSSLContext() {
		EnsureSSLInitialized();				
		const(SSL_METHOD*) method = SSLv23_server_method();
		SSL_CTX* ctx = SSL_CTX_new(method);

		if(!ctx)
			SslHelper.EnforceErrors("Unable to create context.", true); 			

		if(!SslHelper.ReadCertificateAndKey(ctx, Properties.SslCertificatePath, Properties.SslKeyPath))
			throw new SslException("Unable to initialize certificate and/or key.");

		return ctx; 
	}

	private void HandleRequest(RequestInfo* Info) {				
		BackendConnection NextBackend = Info.SiteContaining.Backends.BackendForRequest(cast(HttpRequest)Info.Request);
		if(!NextBackend) {
			debug std.stdio.writeln("No backends found.");
			SendStatusCode(Info.Connection, cast(HttpRequest)Info.Request, 503, "Did not find a server capable of handling your request. Try again in a few moments.", null);
		} else {			
			Info.Backend = NextBackend;			
			/+Info.Connection.Send(cast(ubyte[])"HTTP/1.1 200 OK\r\nConnection: Close\r\nContent-Length: 8\r\n\r\nTesting!").NotifyOnComplete(null, (State, Caller, Type) {
				Info.Connection.Socket.Disconnect("Send complete.", null, null);
			});+/
			//Info.Connection.Send(cast(ubyte[])"HTTP/1.1 200 OK\r\nConnection: Keep-Alive\r\nContent-Length: 8\r\n\r\nTesting!");
			NextBackend.SendRequest(Info.Request);		
		}
	}	

	private void SendStatusCode(HttpConnection Connection, HttpRequest Request, uint StatusCode, string Details, string[string] ExtraHeaders = null) {
		HttpResponse Response = new HttpResponse(Request);
		Response.StatusCode = StatusCode;		
		if(ExtraHeaders)
			foreach(Key, Value; ExtraHeaders)
				Response.AddHeader(Key, Value);
		string ErrorString = HttpUtils.StatusCodeToPhrase(StatusCode);
		string ResponseBody = "<html><head><title>ShardWebServer - Error " ~ to!string(StatusCode) ~ "</title></head><body><h1>Error " ~ to!string(StatusCode) ~ ": " ~ ErrorString ~ "</h1><p>" ~ Details ~ "</p></body></html>";
		Response.AppendBody(ResponseBody);
		if(!HttpUtils.IsSuccessfulStatusCode(StatusCode)) {
			string ErrorMsg = "Error " ~ to!string(StatusCode) ~ ": \'" ~ Details ~ "\'";
			Log("warnings", ErrorMsg);
			writefln(ErrorMsg);
		}
		SendResponse(Connection, Response);
	}	

	private static void WriteHeaderString(HttpResponse Response, Buffer SendBuffer) {		
		string Result;
		foreach(string Key, Value; Response.Headers) {
			SendBuffer.Write(Key);
			SendBuffer.Write(": ");
			SendBuffer.Write(Value);
			SendBuffer.Write("\r\n");			
		}		
	}

	private static void WriteStringForCookie(in Cookie c, Buffer SendBuffer) {	
		// TODO: Consider checking \r here too. Are there any browsers that allow \r without \n for headers? It does go against the HTTP standard...	
		if(Contains(c.Name, '\n') || Contains(c.Value, '\n'))
			throw new HttpException("HTTP Response Splitting exploit found in cookie.");		
		SendBuffer.Write(c.Name);
		SendBuffer.Write('=');
		SendBuffer.Write(c.Value);		
		if(c.Domain)
			SendBuffer.Write("; " ~ c.Domain);
		if(c.Expires != DateTime.min)
			SendBuffer.Write("; Expires=" ~ DateParse.toHttp(c.Expires));
		if((c.Flags & CookieFlags.HttpOnly) != 0)
			SendBuffer.Write("; HttpOnly");
		if((c.Flags & CookieFlags.SecureOnly) != 0)
			SendBuffer.Write("; Secure");				
	}

	private void SendResponse(HttpConnection Connection, HttpResponse Response) {
		if(!Connection.Socket.IsAlive()) {
			RequestCancelled.Execute(cast(HttpRequest)Response.Request);
			return;
		}
		// Rough estimate of how much space we need to store the response data.
		Buffer SendBuffer = BufferPool.Global.Acquire(cast(size_t)(Response.Content.length * 1.2f + 1024));

		scope(exit) {
			Response.NotifyComplete();
			synchronized(RequestToInfoLock)
				RequestToInfo.remove(Response.Request);						
		}
		SendBuffer.Write("HTTP/1.1 " ~ to!string(Response.StatusCode) ~ HttpUtils.StatusCodeToPhrase(Response.StatusCode) ~ "\r\n");		
		
		if(!Response.Headers.Contains("Server"))			
			Response.AddHeader("Server", "ShardWebServer");
		if(!Response.Headers.Contains("Connection"))
			Response.AddHeader("Connection", "Keep-Alive");						
		if(Response.ContentEncoding == EncodingType.Deflate)
			Response.AddHeader("Content-Encoding", "deflate");
		else if(Response.ContentEncoding == EncodingType.Gzip)
			Response.AddHeader("Content-Encoding", "gzip");
		Response.AddHeader("Content-Length", to!string(Response.Content.length));		
		WriteHeaderString(Response, SendBuffer);
		//bool IsSSL = Connection.IsSSL;
		bool IsSSL = false;
		foreach(c; Response.CookiesToSend) {
			if((c.Flags & CookieFlags.SecureOnly) != 0 && !IsSSL)
				throw new InsecureCookieException();	
			SendBuffer.Write("Set-Cookie: ");
			WriteStringForCookie(c, SendBuffer); // We can't just add a header because we'd overwrite an existing one.						
			SendBuffer.Write("\r\n");			
		}		
		SendBuffer.Write("\r\n");
		SendBuffer.Write(Response.Content);		
		if(IsSSL) {					
			throw new NotImplementedError("SSL not yet supported.");
			/*int SendValue = SSL_write(Connection.Value.SSLInfo, SendBuffer.Data.ptr, cast(int)SendBuffer.Count);
			size_t Offset = 0;
			if(SendValue < 0)
				Log("weberrors", "Response may have been cut off. Error code for SendValue was negative. Send error code was " ~ to!string(SSL_get_error(Connection.Value.SSLInfo, SendValue)));*/
		} else {
			// TODO: Split into chunks.

			// TODO: Instead of duplicating, notify on complete and release there. But need a way of passing in Buffer then...
			// Also, make sure to remove the release on scope exit at that point.
			// Either way, this is a significant performance hit at the moment.				
			IOAction Action = Connection.Send(SendBuffer.Data);			
			Action.NotifyOnComplete(Untyped(SendBuffer), &OnSendComplete);
		}			
		// TODO: Support Connection: Close. Or rather, just not Keep-Alive.		
		string ConnType = Response.Request.Headers.Get("Connection", "Close");
		if(!EqualsInsensitiveAscii(ConnType, "Keep-Alive"))
			Connection.Socket.Disconnect("Keep-Alive not enabled.", null, null);
	}

	private void OnSendComplete(Untyped State, AsyncAction Action, CompletionType Status) {
		Buffer buff = cast(Buffer)State;
		BufferPool.Global.Release(buff);
	}

	private static bool IsHostTLD(in char[] HostName) {
		string HostString = cast(string)HostName;
		size_t IndexSlash = IndexOf(HostString, '/');
		if(IndexSlash != -1)
			HostString = HostString[0..IndexSlash];
		
		size_t NumDots = Count!("a == '.'")(HostString);		
		return NumDots > 1;
	}		

	bool ParseInput(HttpConnection Connection, char[] Data, out StringMap ProtocolParams, out StringMap QueryParams, out Cookie[] Cookies, out string FailReason) {
	//	Timer t = Timer.StartNew();
		bool Result;
		FailReason = null;
		try {
			ProtocolParams = new StringMap();
			QueryParams = new StringMap();
			size_t Current = 0;
			char* DPTR = Data.ptr;
			char[] Content;
			for(size_t i = 0; i < Data.length; i++) {
				char c = *(DPTR + i);
				if(c == '\r' && i < Data.length - 1 && *(DPTR + i + 1) == '\n') {			
					bool IsFirstLine = Current == 0;
					char[] Line = Data[Current..i];
					Current = i;				
					if(!ParseParameter(Line, IsFirstLine, ProtocolParams, Cookies, FailReason))
						return false;
					if(i < Data.length - 3 && *(DPTR + i + 2) == '\r' && *(DPTR + i + 3) == '\n') {
						if(i < Data.length - 4)
							Content = Data[i + 4..$];
						else
							Content = new char[0];
						break;
					}
				}
			}

			string Method = ProtocolParams.Get("method", "GET");
			if(EqualsInsensitiveAscii(Method, "POST")) {
				string Fixed = FixQueryStringForPost(ProtocolParams, Content);
				if(Fixed is null) {
					FailReason = "Unable to mix get parameters with post parameters.";
					return false;
				}
				ProtocolParams.Set("query_string", Fixed);			
			}

			if(!ParseQueryParams(cast(char[])ProtocolParams.Get!string("query_string"), ProtocolParams, QueryParams, Connection, FailReason))
				return false;
			Result = true;
		} catch (Throwable Ex) {
			ErrorInfo ei;
			ei.Details = "ParseInput Exception:\r\n" ~ to!string(Ex);
			ei.RemoteIP = Connection.Socket.RemoteAddress;
			ErrorHandler.NotifyError(ei);
			FailReason = "An internal server error occurred while parsing your request.";
			Result = false;
		}		
		//t.StopPrint("ParseInput");
		return Result;
	}

	string FixQueryStringForPost(StringMap ProtocolParams, char[] Content) {
		string Query = ProtocolParams.Get!string("query_string");
		if(Query.IndexOf('?') == -1)
			Query ~= '?' ~ cast(immutable)Content;
		else
			return null; // We don't want them to be able to mix get with post. That would be bad for CSRF.
		return Query;
	}
	
	bool ParseQueryParams(char[] QueryString, StringMap ProtocolParams, StringMap QueryParams, HttpConnection Connection, ref string FailReason) {
		size_t IndexParam = IndexOf(QueryString, '?');
		char[] UriPart;
		char[] QueryPart;
		if(IndexParam == -1) {
			UriPart = QueryString;
			QueryPart = new char[0];
		} else {
			UriPart = QueryString[0..IndexParam];
			if(IndexParam < QueryString.length - 1)
				QueryPart = QueryString[IndexParam + 1..$];
			else
				QueryPart = new char[0];
		}
		string URI = cast(immutable)UriPart;
		try {
			URI = decode(URI);
		} catch {
			FailReason = "Failed to decode parameters.";
			return false;
		}
		if(!HttpUtils.ValidateURI(URI)) {
			FailReason = "Invalid request URI.";
			return false;
		}
		ProtocolParams.Set("uri", URI);

		// TODO: Create split function that allows both & and ;, since a semicolon can technically be used in place of ampersand.
		char[][] ValuePart = split(QueryPart, "&");
		for(size_t i = 0; i < ValuePart.length; i++) {
			char[] KVP = ValuePart[i];
			size_t IndexEquals = KVP.IndexOf('=');
			string Key, Value;
			if(IndexEquals == -1 || IndexEquals >= KVP.length - 1) {					
				Key = cast(string)KVP;
				Value = null;
			} else {
				char[] KeyVal = KVP[0..IndexEquals];				
				Key = cast(string)KeyVal;								
				Value = cast(immutable)KVP[IndexEquals+1..$];				
			}						
			try {
				Key = decode(Key);				
				if(Value)
					Value = decode(Value);
			} catch (URIerror Ex) {
				string ErrorMessage = "Unable to decode value \'" ~ to!string(Value) ~ "\' for URI \'" ~ URI ~ "\'.";
				ErrorInfo ei;
				ei.Url = URI;
				ei.Details = ErrorMessage;
				ei.Parameters = QueryParams;
				ei.RemoteIP = Connection.Socket.RemoteAddress;
				ErrorHandler.NotifyError(ei);		
				FailReason = "Failed to decode parameter data.";			
				return false;
			}			
			QueryParams.Set(Key, Value);
		}
		return true;
	}

	bool ParseParameter(char[] Line, bool IsFirstProtocolLine, StringMap ParamMap, ref Cookie[] Cookies, ref string FailReason) {
		if(IsFirstProtocolLine) {
			// METHOD URI Http/Version
			char[][] Parts = split(Line, " ");
			if(Parts.length != 3) {
				FailReason = "Expected three-part status line, consisting of Method, URI, and Version.";
				return false;
			}
			char[] Method = Parts[0];
			char[] URI = Parts[1];
			char[] Version = Parts[2];
			//if(Method != "GET" && Method != "POST")
				//return false; // Do this later.
			ParamMap.Set("method", cast(immutable)Method);
			ParamMap.Set("query_string", cast(immutable)URI);			
			ParamMap.Set("version", cast(immutable)Version);
		} else {
			size_t IndexDD = Line.IndexOf(':');
			char[] Key, Value;
			if(IndexDD == -1 || IndexDD >= Line.length - 1) {
				Value = null;
				Key = Line;
			} else {
				Key = strip(Line[0..IndexDD]);
				Value = strip(Line[IndexDD + 1..$]);
			}					
			if(Key && Value && EqualsInsensitiveAscii(Key, "Cookie")) {
				if(Cookies.length > Properties.MaxCookiesPerRequest) {	
					FailReason = "Too many cookies set for a single request.";
					return false;
				} 
				string[] ValueSplit = cast(string[])split(Value, ";");
				foreach(string KeyValue; ValueSplit) {
					size_t IndexEquals = KeyValue.IndexOf('=');
					if(IndexEquals == -1 || IndexEquals == KeyValue.length - 1) {
						FailReason = "No value found for a cookie.";
						return false;
					}
					string CookieKey = strip(KeyValue[0..IndexEquals]);
					string CookieValue = strip(KeyValue[IndexEquals + 1..$]);
					Cookie c = Cookie(CookieKey, CookieValue, null, DateTime.min, CookieFlags.None, CookieSender.Client);
					Cookies ~= c;
				}
			} else {				
				// TODO: Decide how to handle IPs.
				// Gonna need to change HostNames for this, and allow Bindings that uses either a HostName or IP, and an array of them.
				if(Key && Value && EqualsInsensitiveAscii(Key, "Host")) {
					Value = GetFixedHost(Value);
				}				
				ParamMap.Set(cast(immutable)Key, cast(immutable)Value);
			}
		}
		return true;
	}

	private static char[] GetFixedHost(char[] Host) {	
		size_t IndexPort = IndexOf(Host, ':');
		if(IndexPort != -1)
			Host = Host[0..IndexPort];			
		if(!IsHostTLD(Host))
			Host = SiteManager.GetFixedHostName(Host);
		if(startsWith(Host, "http://"))
			Host = Host["http://".length .. $];		
		return Host;
	}
}