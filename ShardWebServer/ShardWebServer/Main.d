﻿module Main;
private import ShardData.PgSql.PgSqlCommand;
private import ShardData.DbCommand;
private import ShardTools.Mixins;
private import std.parallelism;
private import std.stream;
private import ShardTools.ConcurrentStack;
private import ShardWeb.SiteManager;
private import ShardTools.PathTools;
import ShardWeb.Site;
import std.datetime;
import ShardTools.Logger;
import core.thread;
import core.time;
import ShardWebServer.HttpServerProperties;
import ShardWebServer.HttpServer;
import ShardWeb.HttpResponse;
import ShardWeb.Page;
import ShardWeb.HttpRequestHandler;
import core.runtime;
import ShardTools.Library;
//import FakeFile;

import std.stdio;
import std.c.stdlib;
import std.exception;
import std.conv;
import ShardSite.HomePage;
import std.string;

/// The main entry point for a console program.
///	Params:
///		Args = The commandline arguments to the program, including program name.
int main(string[] Args) {		
	/*writeln("Hello world!");
	//Library PageLib = new Library("TestPages.dll");
	//PageLib.Load();
	void* Handle = enforce(Runtime.loadLibrary("TestPages.dll"));
	Dispatcher Dispatch = new Dispatcher();
	Dispatch.AddPagesFromModules();
	enforce(Runtime.unloadLibrary(Handle));*/
	/+ConcurrentStack!int Test = new ConcurrentStack!int();
	for(int i = 0; i < 10000; i++)
		Test.Push(i);+/	
	defaultPoolThreads = 64;
	//debug defaultPoolThreads = 1;
	version(Windows) {
		// TODO...
	} else {
		PathTools.SetWorkingDirectory(PathTools.ApplicationDirectory);	
	}		
	SiteManager.SiteAdded.Add((Site site) { writefln("%s", "Added site " ~ site.Name ~ " located at \'" ~ to!string(site.HostNames) ~ "\' with root directory of \'" ~ site.RootDirectory ~ "\'."); });
	SiteManager.SiteRemoved.Add((Site site) { writefln("%s", "Removed site " ~ site.Name ~ " located at \'" ~ to!string(site.HostNames) ~ "\'."); });	
	HttpServerProperties Props = new HttpServerProperties();
	version(Windows) {
		Props.SslCertificatePath = PathTools.MakeAbsolute("../../shardweblocal.cer");
		Props.SslKeyPath = PathTools.MakeAbsolute("../../shardweblocal.key");		
	} else {
		Props.SslCertificatePath = PathTools.MakeAbsolute("../../shardweb.cer");
		Props.SslKeyPath = PathTools.MakeAbsolute("../../shardweb.key");
	}
	Props.AllowSSL = true;
	foreach(string Arg; Args) {
		if(startsWith(Arg, "port=")) {
			Props.Port = to!ushort(Arg["port=".length .. $]);
		}
	}	
	//Props.Port = 6101;
	// TODO: lower this. Just for scaling tests.
	Props.MaxConnectionsPerUser = 65535; // TODO: Remember to remove this!
	Props.NumWorkers = 64;
	//debug Props.NumWorkers = 1;
	// TODO: Stop logging post parameters.	
	HttpServer Server = new HttpServer(Props);
	Server.Listen();		
	writefln("Active Sites:");
	foreach(Site site; SiteManager.AllSites)
		writefln("\t" ~ site.Name ~ " - HostNames: \'" ~ to!string(site.HostNames) ~ "\' - Root Directory: \'" ~ site.RootDirectory ~ "\'.");
	bool IsListening = true;
	while(IsListening) {
		core.thread.Thread.sleep(dur!"msecs"(100));		
	}	
	return 0;	
} 