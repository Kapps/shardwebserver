module HttpConnectionTest;
import std.stdio;
import OpenSSL;
import std.datetime;
import std.socket;
import std.conv : to;
import std.stdio : writefln;
static import core.time;

enum SslStatus {
	Unknown = 0,
	NotSSL = 1,
	WantsIO = 2,
	Ready = 4
}

/// Represents a single connection to the server.
class HttpConnection  {

public:
	/// Initializes a new instance of the HttpConnection object.
	/// Params:
	///		TimeoutTime = The amount of time this connection needs to idle before being closed.
	/// 	Connection = The underlying socket for this connection.
	/// 	ssl = Provides information about the SSL protocol for this connection, or null if this connection is not using SSL.
	this(SysTime TimeoutTime, Socket Connection, SSL* ssl) {		
		this._TimeoutTime = TimeoutTime;
		this._Socket = Connection;
		this._ssl = ssl;
		this.SyncLock = new	Object();
		if(ssl)
			_Status = SslStatus.Unknown;
		else
			_Status = SslStatus.NotSSL;
	}

	/// A lock object used for when synchronization of something required by this connection is needed.
	/// One example is any SSL calls (such as get last error following a receive or send).
	const Object SyncLock;

	/// Gets or sets the time that this connection should time out at.
	@property SysTime TimeoutTime() {
		return _TimeoutTime;
	}

	/// Ditto
	@property void TimeoutTime(SysTime Time) {
		_TimeoutTime = Time;
	}

	/// Gets the underlying socket for this connection.
	@property Socket Connection() {
		return _Socket;		
	}

	/// Delays the timeout time for the given duration.
	/// Params:
	/// 	Time = The time to delay the timeout for.
	@property void DelayTimeout(core.time.Duration Time) {
		_TimeoutTime = _TimeoutTime + Time;		
	}

	/// Sends the given data to this connection, splitting it as appropriate.
	/// Params:
	/// 	Data = The data to send.
	void SendResponse(void[] Data) {				
		if(Data.length > 0 && _Socket && _Socket.isAlive)
			_Socket.send(Data);
		// TODO: Handle large files, dispatch them to something else possibly or else just do the transfer async.
	}

	/// Gets a value indicating whether this connection uses SSL.
	@property bool IsSSL() const {
		return _ssl !is null;
	}

	/// Provides SSL properties for this connection, or null if this connection does not use SSL.
	@property SSL* SSLInfo() {
		return _ssl;
	}

	/// Gets the status of the SSL handshake or protocol for this connection.
	@property SslStatus SSLStatus() const {
		return _Status;
	}

	/// Ditto
	@property void SSLStatus(SslStatus Value) {
		_Status = Value;
	}

private:
	Socket _Socket;
	SysTime _TimeoutTime;	
	package SSL* _ssl;
	SslStatus _Status;	
} 

void main() {
	SSL* ssl = new SSL();	
	SysTime Timeout = Clock.currTime();
	Socket Connection = new Socket(AddressFamily.INET, SocketType.STREAM, ProtocolType.TCP);
	HttpConnection http = new HttpConnection(Timeout, Connection, ssl);
	writefln("Done. " ~ to!string(http));
}